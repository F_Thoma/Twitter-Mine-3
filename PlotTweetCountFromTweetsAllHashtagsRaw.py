import os, sys, time, numpy as np, matplotlib.pyplot as plt
import csv
from datetime import datetime
import matplotlib.dates as mdates

import tweetMiningLib as mLib

start_date = '2020-07-01'
end_date = '2020-07-04'
hashtags = ["#MECFS", #"#MEawarenesshour", "#pwME", "#MillionsMissing", "#MyalgicE", "#cfs", "#Spoonie", "#neisvoid"
                    "#LongCovid"
                    ,"#grippe"
                    ,"#Merkel"
                    ,"#Trump"
                    #,"#linksextrem", "#rechtsextrem"
                    ,"#Amthor"
                    #, "#Korruption"
                    , "#Lobby"
                    #, "#Lobbyismus"
                    ]
saveFigure=False
Zeigedifferenz=0

dataSets=[None]*(len(hashtags)+Zeigedifferenz)

cmap = plt.get_cmap('hsv')
for i in range(len(hashtags)):
    TweetCount, dates = mLib.getTweetCountFromSavedTweets(hashtags[i], start_date, end_date)
    dataSets[i] = [mLib.datetimesFromStr(dates, endOffset=-1), TweetCount]
    color = cmap(float(i)/len(dataSets))
    plt.plot(dataSets[i][0], dataSets[i][1], '--' if i%2 else '', c=color, alpha=0.8, linewidth=1.8, label=hashtags[i])

#Differenz
if Zeigedifferenz:
    dataSets[2] = [dataSets[0][0], list(np.array(dataSets[0][1]) - np.array(dataSets[1][1]))]
    i = 2
    runAvgDataSets[i] = mLib.runningAverageDataset(dataSets[i], runAvgCount)
    color = cmap(float(i)/len(dataSets))
    plt.plot(dataSets[i][0], dataSets[i][1], c=color, alpha=0.12, linewidth=0.5)#, label=hashtags[i])
    plt.plot(runAvgDataSets[i][0], runAvgDataSets[i][1], c=color, label='Differenz' +' '+str(runAvgCount)+'d avg', alpha=0.8, linewidth=1.8)


#plt.xaxis.set_major_locator(mdates.MonthLocator())
#plt.xaxis.set_major_formatter(mdates.DateFormatter('%b %y'))
plt.xticks(rotation=90)

plt.subplots_adjust(left=0.105 , bottom=0.20, right=0.985, top=0.95)
plt.legend();
plt.grid(which='major', alpha=0.35, linestyle='--');
plt.ylabel('tweets per day' + (' with ' + hashtags[0] if len(dataSets)<=1 else ''));
if saveFigure:
    plotFilepath = mLib.get_tweets_per_day_generated_plot_file_path(hashtags, start_date, end_date)
    plt.savefig(plotFilepath)
plt.yscale('symlog')
plt.title("Vergleich ausgewählter Twitter Hashtags")
plt.show();
