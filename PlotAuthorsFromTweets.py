import os, sys, time, numpy as np, matplotlib.pyplot as plt
import csv
from datetime import datetime
import matplotlib.dates as mdates
from collections import Counter

import tweetMiningLib as mLib

start_date = '2020-06-01'
end_date = '2020-08-16'
hashtag = '#MECFS'
saveFigure=False
PlotCount = 12
PrintCount = 150
runAvgCount = 14

print(datetime.now())
print('  Reading Files...')
authorPerDayPerTweet, dates = mLib.extractDataFromSavedTweets(hashtag, start_date, end_date, mLib._row_extract_author)

print('  Counting...')
AuthorCounters = [Counter(authorsOnOneDay) for authorsOnOneDay in authorPerDayPerTweet]
totalCounter = mLib.counterSum(AuthorCounters)
totalTweetCount = sum(totalCounter.values())

print('Hashtag:', hashtag)
print('Time:', start_date, 'until', end_date)
print('There are ' + str(len(list(totalCounter))) + ' authors and ' + str(totalTweetCount) + ' tweets')
print('Top authors: ')
print('Nr.', 'Amount (rel., abs.)', 'author', sep='\t')
i =1
for otherHashtag, usage in totalCounter.most_common(PrintCount):
    print('['+str(i)+']', round (usage/totalTweetCount*100, 3), '% ', usage, otherHashtag, sep='\t')
    i+=1
print()


print('  Formatting data...')
def getDataFormatted(counterPerDay, totalCounter, tweetCountPerDay, MaxCount =None):
    days = len(counterPerDay)
    data = dict(totalCounter.most_common(MaxCount))
    
    for hashtag,trash in totalCounter.most_common(MaxCount):
        data[hashtag] = np.zeros(days)
    for i in range(days):
        for hashtag,trash in totalCounter.most_common(MaxCount):
            data[hashtag][i] = (counterPerDay[i][hashtag]/tweetCountPerDay[i] if tweetCountPerDay[i] is not 0 else -1)
    return data
def removeWrongData(data):
    for i in range(len(data[1])-1, -1, -1):
        if data[1][i] < 0:
            data = np.delete(data, i, 1)
    return data

tweetCountPerDay, dates = mLib.getTweetCountFromSavedTweets(hashtag, start_date, end_date)
data = getDataFormatted(AuthorCounters, totalCounter,tweetCountPerDay, PlotCount)

print('  Plotting data...')
plt.figure(figsize=(10,5))

cmap = plt.get_cmap('hsv')
i=0
for otherHashtag, otherHashtagCount in totalCounter.most_common(PlotCount):
    relativeTweetCount = data[otherHashtag]
    dataSet = [mLib.datetimesFromStr(dates, endOffset=-1), relativeTweetCount*100]
    dataSet = removeWrongData(dataSet)
    runAvgDataSet = mLib.runningAverageDataset(dataSet, runAvgCount)
    color = cmap(float(i)/len(data))
    if(len(dataSet[0]) < 200):
        plt.plot(dataSet[0], dataSet[1], c=color, alpha=0.12, linewidth=0.5)#, label=otherHashtag)
    plt.plot(runAvgDataSet[0], runAvgDataSet[1], '--' if i%2 else '', c=color, alpha=0.8, linewidth=1.8,
             label= str(round(totalCounter[otherHashtag]/totalTweetCount*100, 1)) +'% '+otherHashtag)
    i+=1


plt.xticks(rotation=90)

plt.subplots_adjust(left=0.065 , bottom=0.20, right=0.77, top=0.95)
#plt.legend()
plt.legend(loc='upper left', bbox_to_anchor=(1.0, 1.0));
plt.grid(which='major', alpha=0.35, linestyle='--');
plt.ylabel('top '+ str(PlotCount) +' authors of ' + hashtag + ' in %  ('+str(runAvgCount)+'d avg)');
#plt.yscale('symlog')
plt.title('Authors of ' + hashtag)

print(datetime.now())
if saveFigure:
    plotFilepath = mLib.get_tweets_per_day_generated_plot_file_path(hashtags, start_date, end_date)
    plt.savefig(plotFilepath)
plt.show();
