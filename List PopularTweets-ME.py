import os, sys, time, numpy as np, matplotlib.pyplot as plt, LivePlotConfig
import csv
from datetime import datetime, date, timedelta
import matplotlib.dates as mdates
from collections import Counter

import tweetMiningLib as mLib


outputFolder="C:\\Users\\Frederik\\Nextcloud\\Twitter-Mine Auswertungsergebnisse\\ME\\Top Tweets\\"



start_date = '2018-01-01'
end_date = '2020-09-16'

maxTweetCount = 400
minimumPopularity = 20

for hashtag in ['#MECFS', '#MEawarenesshour']:
    mLib.savePopularTweetsInHtmlFile(hashtag = hashtag,
                                     outputFolder = outputFolder,
                                     start_date = '2018-01-01',
                                     end_date = '2020-09-16',
                                     maxTweetCount = maxTweetCount,
                                     minimumPopularity = minimumPopularity)
mLib.savePopularTweetsInHtmlFile(hashtag = '#LongCovid',
                                     outputFolder = outputFolder,
                                     start_date = '2020-05-01',
                                     end_date = '2020-09-16',
                                     maxTweetCount = maxTweetCount,
                                     minimumPopularity = minimumPopularity)
