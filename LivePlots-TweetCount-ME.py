import os, sys, time, numpy as np, matplotlib.pyplot as plt, LivePlotConfig, LivePlotConfig
import csv
from datetime import datetime, date, timedelta
import matplotlib.dates as mdates

import tweetMiningLib as mLib


outputFolder=LivePlotConfig.outputFolder()

start_date = mLib.daysAgo(5*30) #'2020-05-01'
end_date = mLib.yesterday()
hashtags = ["#MECFS", "#MEawarenesshour", #"#pwME", "#MillionsMissing", "#MyalgicE", "#cfs", "#Spoonie", "#neisvoid"
                    "#LongCovid"
                    ]
saveFigure=True

plt.style.use(LivePlotConfig.pltStyle())
plt.subplots(figsize=LivePlotConfig.pltFigSize())

dataSets=[None]*(len(hashtags))

for i in range(len(hashtags)):
    TweetCount, dates = mLib.getTweetCountFromSavedTweets(hashtags[i], start_date, end_date)
    dataSets[i] = [mLib.datetimesFromStr(dates), TweetCount]
    color = mLib.getHsvColor(i, len(dataSets))
    plt.plot(dataSets[i][0], dataSets[i][1], '--' if i%2 and False else '', c=color, alpha=0.8, linewidth=1.8, label=hashtags[i])

#plt.xticks(rotation=90)

plt.subplots_adjust(left=0.050, bottom=0.08, right=0.985, top=0.92)
plt.legend();
plt.grid(which='major', alpha=0.35, linestyle='--');
plt.ylabel('tweets per day' + (' with ' + hashtags[0] if len(dataSets)<=1 else ''));
#plt.yscale('symlog')
plt.title("Vergleich ausgewählter Twitter Hashtags")

if saveFigure:
    FilePath = outputFolder + 'Tweet count ' + ' '.join(hashtags).replace('#','') + LivePlotConfig.fileType()
    plt.savefig(FilePath, dpi=LivePlotConfig.dpi())
    print('Saved Figure: '+ FilePath)
#plt.show();
