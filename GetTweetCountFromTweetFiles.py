import os, sys, time, numpy as np, matplotlib.pyplot as plt
import csv
from datetime import datetime
import matplotlib.dates as mdates

import tweetMiningLib as mLib

start_date = '2020-05-15'
end_date = mLib.yesterday()
hashtag = "#LongCovid"
saveCsv=True
saveFigure=False

## get and save data

TweetCount, dates = mLib.getTweetCountFromSavedTweets(hashtag, start_date, end_date)
print(dates)
if saveCsv:
    mLib.saveTweetsPerDay(mLib.get_tweets_per_day_file_path(hashtag, start_date, end_date),
                      dates, TweetCount)
## show plot

mLib.plotTweetPerDayData(TweetCount, dates, hashtag)
