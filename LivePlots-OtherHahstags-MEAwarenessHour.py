import os, sys, time, numpy as np, matplotlib.pyplot as plt, LivePlotConfig
import csv
from datetime import datetime, date, timedelta
import matplotlib.dates as mdates
from collections import Counter

import tweetMiningLib as mLib

outputFolder=LivePlotConfig.outputFolder()



start_date = '2020-10-23'
end_date = mLib.yesterday()
hashtag = '#MEawarenesshour'
saveFigure=True

PlotCount = 12
PrintCount = 30
runAvgCount = 7


def getHashtagsWithDifferentWriting(counter):
    corrections = {}
    hashtags = [hashtag for hashtag, count in counter.most_common()]
    hashtagComparable = [hashtag.casefold() for hashtag in hashtags]
    for i in range(len(hashtags)):
        for j in range(i):
            if(hashtagComparable[i] == hashtagComparable[j]):
                corrections[hashtags[i]] = hashtags[j]
                break
    return corrections
def cleanHashtagsWithDifferentWriting(counter, corrections):
    for hashtag in corrections:
        moreCommonHashtag = corrections[hashtag]
        counter[moreCommonHashtag] = counter.get(hashtag, 0) + counter.get(moreCommonHashtag, 0)
        del counter[hashtag]

print(datetime.now())
print('Reading Files...')
otherHashtags, dates = mLib.extractDataFromSavedTweets(hashtag, start_date, end_date, mLib._row_extract_hashtags)

print('Counting...')
hashtagsPerDay = [mLib.listSum(hashtagsPerOneDayPerTweet) for hashtagsPerOneDayPerTweet in otherHashtags]
hashtagCounters = [Counter(hashtagsPerOneDay) for hashtagsPerOneDay in hashtagsPerDay]
totalCounter = mLib.counterSum(hashtagCounters)

print(datetime.now())
print('Finding duplicate hashtags (different capitalization)...')
print('  There are ' + str(len(list(totalCounter))) + ' hashtags')
corrections = getHashtagsWithDifferentWriting(totalCounter)
print(datetime.now())
print('Correcting hashtags...')
cleanHashtagsWithDifferentWriting(totalCounter, corrections)
hashtag = corrections.get(hashtag, hashtag)
totalTweetCount = totalCounter[hashtag]
del totalCounter[hashtag]
print('  There are ' + str(len(list(totalCounter))) + ' unique hashtags')
i=0
for counter in hashtagCounters:
    cleanHashtagsWithDifferentWriting(counter, corrections)
    del counter[hashtag]
    i+=1

print('Formatting data...')
def removeWrongData(data):
    for i in range(len(data[1])-1, -1, -1):
        if data[1][i] < 0:
            data = np.delete(data, i, 1)
    return data

tweetCountPerDay, dates = mLib.getTweetCountFromSavedTweets(hashtag, start_date, end_date)
data           = mLib.getRelativeAmountFromCounters(hashtagCounters, totalCounter, tweetCountPerDay, PlotCount)
runningAvgData = mLib.getRelativeAmountFromCounters(hashtagCounters, totalCounter, tweetCountPerDay, PlotCount, avgCount=runAvgCount)

print('Top hashtags: ')
for otherHashtag, usage in totalCounter.most_common(PrintCount):
    print(round (usage/totalTweetCount*100, 3), '% ', usage, otherHashtag)

print('Plotting data...')
plt.subplots(figsize=(16,5.2))

i=0
for otherHashtag, otherHashtagCount in totalCounter.most_common(PlotCount):
    dataSet = [mLib.datetimesFromStr(dates, endOffset=-1), data[otherHashtag]*100]
    dataSet = removeWrongData(dataSet)
    runAvgDataSet = [mLib.runAvgXcompensation(mLib.datetimesFromStr(dates, endOffset=-1), runAvgCount), runningAvgData[otherHashtag]*100]
    runAvgDataSet = removeWrongData(runAvgDataSet)
    color = mLib.getHsvColor(i, len(data))
    if(len(dataSet[0]) < 120):
        plt.plot(dataSet[0], dataSet[1], '--' if i%2 else '', c=color, alpha=0.12, linewidth=0.6)#, label=otherHashtag)
    plt.plot(runAvgDataSet[0], runAvgDataSet[1], '--' if i%2 else '', c=color, alpha=0.8, linewidth=1.8,
             label= str(round(totalCounter[otherHashtag]/totalTweetCount*100, 1)) +'% '+otherHashtag)
    i+=1

#plt.xaxis.set_major_locator(mdates.MonthLocator())
#plt.xaxis.set_major_formatter(mdates.DateFormatter('%b %y'))
#plt.xticks(rotation=90)

plt.subplots_adjust(left=0.050 , bottom=0.07, right=0.79, top=0.95)
#plt.legend()
plt.legend(loc='upper left', bbox_to_anchor=(1.0, 1.0));
plt.grid(which='major', alpha=0.35, linestyle='--');
plt.ylabel('top '+ str(PlotCount) +' hashtags used with ' + hashtag + ' in %  ('+str(runAvgCount)+'d avg)');

#plt.yscale('symlog')
plt.title(hashtag)
print(datetime.now())

if saveFigure:
    FilePath = outputFolder + 'Other hashtags used with ' + hashtag.replace('#','') + LivePlotConfig.fileType()
    plt.savefig(FilePath, dpi=LivePlotConfig.dpi())
    print('Saved Figure: '+ FilePath)
#plt.show();
