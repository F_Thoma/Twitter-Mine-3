import os, sys, time, numpy as np, matplotlib.pyplot as plt, LivePlotConfig, LivePlotConfig
import csv
from datetime import datetime, date, timedelta
import matplotlib.dates as mdates

import tweetMiningLib as mLib


outputFolder=LivePlotConfig.outputFolder()

start_date = '2019-11-20' #mLib.daysAgo(120)
end_date = mLib.yesterday()
hashtags = ["#MECFS"
    ,"#MEawarenesshour"
    ,"#LongCovid"
    ,"#pwME", "#MillionsMissing", "#MyalgicE", "#cfs", "#Spoonie", "#neisvoid", "#MemesForME", "#EndMeCfs2021", "#MyalgicEncephalomyelitis", "#ChronicFatigueSyndrome", "#MEAwarenessDay", "#MEAwarenessWeek", "#MEAwarenessMonth", "#MillionsMore"
    ,"#chronicCovid", "#longHaulers", "#longHauler"
                    ]

runningAverageSpan = 7

saveFigure=True

plt.style.use(LivePlotConfig.pltStyle())
plt.subplots(figsize=LivePlotConfig.pltFigSize())

dataSets=[None]*(len(hashtags))

for i in range(len(hashtags)):
    TweetCount, dates = mLib.getTweetCountFromSavedTweets(hashtags[i], start_date, end_date)
    dataSets[i] = mLib.runningAverageDataset([mLib.datetimesFromStr(dates), TweetCount], runningAverageSpan)
    color = mLib.getHsvColor(i, len(dataSets))
    plt.plot(dataSets[i][0], dataSets[i][1], '--' if i%2 and True else '', c=color, alpha=0.8, linewidth=1.8, label=hashtags[i])

#plt.xticks(rotation=90)

plt.subplots_adjust(left=0.050, bottom=0.08, right=0.855, top=0.92)
plt.legend(loc='upper left', bbox_to_anchor=(1.0, 1.1), prop={'size': 9});
plt.grid(which='major', alpha=0.35, linestyle='--');
plt.ylabel('tweets per day' + (' with ' + hashtags[0] if len(dataSets)<=1 else '') + ' (' + str(runningAverageSpan) + ' d avg)');
#plt.yscale('symlog')
plt.title("Vergleich aller gemessenen Twitter Hashtags (Langzeit)")

if saveFigure:
    FilePath = outputFolder + 'Tweet count All' + ' longterm' + LivePlotConfig.fileType()
    plt.savefig(FilePath, dpi=LivePlotConfig.dpi())
    print('Saved Figure: '+ FilePath)
#plt.show();
