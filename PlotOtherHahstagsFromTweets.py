import os, sys, time, numpy as np, matplotlib.pyplot as plt
import csv
from datetime import datetime
import matplotlib.dates as mdates
from collections import Counter

import tweetMiningLib as mLib

start_date = '2020-06-21'
end_date = '2020-08-10'
'''
hashtag = [
            #"#MECFS",
            #"#MEawarenesshour",
            #"#pwME",
            #"#MillionsMissing",
            #"#MyalgicE",
            #"#cfs",
            #"#Spoonie",
            #"#neisvoid",
            #"#linksextrem",
            #"#rechtsextrem",
            "#Amthor",
            "#Korruption",
            "#Lobby"
            ]#'''
hashtag = '#LongCovid'
saveFigure=False
PlotHashtagCount = 10
PrintHashtagCount = 80
runAvgCount = 7


def getHashtagsWithDifferentWriting(counter):
    corrections = {}
    hashtags = [hashtag for hashtag, count in counter.most_common()]
    hashtagComparable = [hashtag.casefold() for hashtag in hashtags]
    for i in range(len(hashtags)):
        for j in range(i):
            if(hashtagComparable[i] == hashtagComparable[j]):
                corrections[hashtags[i]] = hashtags[j]
                break
    return corrections
def cleanHashtagsWithDifferentWriting(counter, corrections):
    for hashtag in corrections:
        moreCommonHashtag = corrections[hashtag]
        counter[moreCommonHashtag] = counter.get(hashtag, 0) + counter.get(moreCommonHashtag, 0)
        del counter[hashtag]

print(datetime.now())
print('Reading Files...')
otherHashtags, dates = mLib.extractDataFromSavedTweets(hashtag, start_date, end_date, mLib._row_extract_hashtags)

print('Counting...')
hashtagsPerDay = [mLib.listSum(hashtagsPerOneDayPerTweet) for hashtagsPerOneDayPerTweet in otherHashtags]
hashtagCounters = [Counter(hashtagsPerOneDay) for hashtagsPerOneDay in hashtagsPerDay]
totalCounter = mLib.counterSum(hashtagCounters)

print(datetime.now())
print('Finding duplicate hashtags (different capitalization)...')
print('  There are ' + str(len(list(totalCounter))) + ' hashtags')
corrections = getHashtagsWithDifferentWriting(totalCounter)
#corrections['#spoonie'] = '#Spoonies/#spoonie'
#corrections['#Spoonies'] = '#Spoonies/#spoonie'
#corrections['#COVID19'] = '#COVID19/#COVID'
#corrections['#COVID'] = '#COVID19/#COVID'
#corrections['#CDU'] = '#CDU/#CSU'
#corrections['#CSU'] = '#CDU/#CSU'
#corrections[''] = ''
print(datetime.now())
print('Correcting hashtags...')
cleanHashtagsWithDifferentWriting(totalCounter, corrections)
hashtag = corrections.get(hashtag, hashtag)
totalTweetCount = totalCounter[hashtag]
del totalCounter[hashtag]
print('  There are ' + str(len(list(totalCounter))) + ' unique hashtags')
i=0
for counter in hashtagCounters:
    cleanHashtagsWithDifferentWriting(counter, corrections)
    del counter[hashtag]
    i+=1

print('Formatting data...')
def getDataFormatted(counterPerDay, totalCounter, tweetCountPerDay, MaxCount =None):
    days = len(counterPerDay)
    data = dict(totalCounter.most_common(MaxCount))
    
    for hashtag,trash in totalCounter.most_common(MaxCount):
        data[hashtag] = np.zeros(days)
    for i in range(days):
        for hashtag,trash in totalCounter.most_common(MaxCount):
            data[hashtag][i] = (counterPerDay[i][hashtag]/tweetCountPerDay[i] if tweetCountPerDay[i] is not 0 else -1)
    return data
def removeWrongData(data):
    for i in range(len(data[1])-1, -1, -1):
        if data[1][i] < 0:
            data = np.delete(data, i, 1)
    return data

tweetCountPerDay, dates = mLib.getTweetCountFromSavedTweets(hashtag, start_date, end_date)
data = getDataFormatted(hashtagCounters, totalCounter,tweetCountPerDay, PlotHashtagCount)

print('Top hashtags: ')
for otherHashtag, usage in totalCounter.most_common(PrintHashtagCount):
    print(round (usage/totalTweetCount*100, 3), '% ', usage, otherHashtag)

print('Plotting data...')
plt.figure(figsize=(10,5))

cmap = plt.get_cmap('hsv')
i=0
for otherHashtag, otherHashtagCount in totalCounter.most_common(PlotHashtagCount):
    relativeTweetCount = data[otherHashtag]
    dataSet = [mLib.datetimesFromStr(dates, endOffset=-1), relativeTweetCount*100]
    dataSet = removeWrongData(dataSet)
    runAvgDataSet = mLib.runningAverageDataset(dataSet, runAvgCount)
    color = cmap(float(i)/len(data))
    if(len(dataSet[0]) < 200):
        plt.plot(dataSet[0], dataSet[1], c=color, alpha=0.12, linewidth=0.5)#, label=otherHashtag)
    plt.plot(runAvgDataSet[0], runAvgDataSet[1], '--' if i%2 else '', c=color, alpha=0.8, linewidth=1.8,
             label= str(round(totalCounter[otherHashtag]/totalTweetCount*100, 1)) +'% '+otherHashtag)
    i+=1

#plt.xaxis.set_major_locator(mdates.MonthLocator())
#plt.xaxis.set_major_formatter(mdates.DateFormatter('%b %y'))
plt.xticks(rotation=90)

plt.subplots_adjust(left=0.065 , bottom=0.20, right=0.77, top=0.95)
#plt.legend()
plt.legend(loc='upper left', bbox_to_anchor=(1.0, 1.0));
plt.grid(which='major', alpha=0.35, linestyle='--');
plt.ylabel('top '+ str(PlotHashtagCount) +' hashtags used with ' + hashtag + ' in %  ('+str(runAvgCount)+'d avg)');
if saveFigure:
    plotFilepath = mLib.get_tweets_per_day_generated_plot_file_path(hashtags, start_date, end_date)
    plt.savefig(plotFilepath)
#plt.yscale('symlog')
plt.title(hashtag)

print(datetime.now())
plt.show();
