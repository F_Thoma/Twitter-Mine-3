import os, sys, time, numpy as np, matplotlib.pyplot as plt, LivePlotConfig
import csv
from datetime import datetime, date, timedelta
import matplotlib.dates as mdates
from collections import Counter

import tweetMiningLib as mLib

outputFolder=LivePlotConfig.outputFolder()



start_date = '2020-05-01'
end_date = mLib.yesterday()
hashtag = '#MECFS'

saveFigure=True

PlotCount = 12
PrintCount = 30

runAvgCount = 7

print(datetime.now())
print('  Reading Files...')
authorPerDayPerTweet, dates = mLib.extractDataFromSavedTweets(hashtag, start_date, end_date, mLib._row_extract_author)

print('  Counting...')
AuthorCounters = [Counter(authorsOnOneDay) for authorsOnOneDay in authorPerDayPerTweet]
totalCounter = mLib.counterSum(AuthorCounters)
totalTweetCount = sum(totalCounter.values())

print('Hashtag:', hashtag)
print('Time:', start_date, 'until', end_date)
print('There are ' + str(len(list(totalCounter))) + ' authors and ' + str(totalTweetCount) + ' tweets')
print('Top authors: ')
print('Nr.', 'Amount (rel., abs.)', 'author', sep='\t')
i =1
for otherHashtag, usage in totalCounter.most_common(PrintCount):
    print('['+str(i)+']', round (usage/totalTweetCount*100, 3), '% ', usage, otherHashtag, sep='\t')
    i+=1
print()


print('  Formatting data...')
def removeWrongData(data):
    for i in range(len(data[1])-1, -1, -1):
        if data[1][i] < 0:
            data = np.delete(data, i, 1)
    return data

tweetCountPerDay, dates = mLib.getTweetCountFromSavedTweets(hashtag, start_date, end_date)
data           = mLib.getRelativeAmountFromCounters(AuthorCounters, totalCounter, tweetCountPerDay, PlotCount)
runningAvgData = mLib.getRelativeAmountFromCounters(AuthorCounters, totalCounter, tweetCountPerDay, PlotCount, avgCount=runAvgCount)

print('  Plotting data...')
plt.style.use(LivePlotConfig.pltStyle())
plt.subplots(figsize=LivePlotConfig.pltFigSize())

i=0
for otherHashtag, otherHashtagCount in totalCounter.most_common(PlotCount):
    dataSet = [mLib.datetimesFromStr(dates), data[otherHashtag]*100]
    dataSet = removeWrongData(dataSet)
    runAvgDataSet = [mLib.runAvgXcompensation(mLib.datetimesFromStr(dates), runAvgCount), runningAvgData[otherHashtag]*100]
    runAvgDataSet = removeWrongData(runAvgDataSet)
    color = mLib.getHsvColor(i, len(data))
    if(len(dataSet[0]) < 60):
        plt.plot(dataSet[0], dataSet[1], c=color, alpha=0.2, linewidth=0.7)#, label=otherHashtag)
    plt.plot(runAvgDataSet[0], runAvgDataSet[1], '--' if i%2 else '', c=color, alpha=0.8, linewidth=1.8,
             label= str(round(totalCounter[otherHashtag]/totalTweetCount*100, 1)) +'% '+otherHashtag)
    i+=1


#plt.xticks(rotation=90)

plt.subplots_adjust(left=0.050 , bottom=0.08, right=0.83, top=0.95)
#plt.legend()
plt.legend(loc='upper left', bbox_to_anchor=(1.0, 1.0));
plt.grid(which='major', alpha=0.35, linestyle='--');
plt.ylabel('top '+ str(PlotCount) +' authors of ' + hashtag + ' in %  ('+str(runAvgCount)+'d avg)');
#plt.yscale('symlog')
plt.title('Authors of ' + hashtag)
print(datetime.now())
if saveFigure:
    FilePath = outputFolder + 'Authors of ' + hashtag.replace('#','') + LivePlotConfig.fileType()
    plt.savefig(FilePath, dpi=LivePlotConfig.dpi())
    print('Saved Figure: '+ FilePath)
#plt.show();
