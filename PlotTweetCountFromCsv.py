import os, sys, time, numpy as np, matplotlib.pyplot as plt
import csv

import tweetMiningLib as mLib

start_date = '2018-07-01'
end_date = '2020-08-06'
hashtag = "#Amthor"
saveFigure=False

CsvFilename = mLib.get_tweets_per_day_file_path(hashtag, start_date, end_date)
saveFigureAs= mLib.get_tweets_per_day_generated_plot_file_path(hashtag, start_date, end_date) if saveFigure else ''


data, header = mLib.readTweetsPerDay(CsvFilename)

mLib.plotTweetPerDayData(data[1], start_date, end_date, hashtag, saveFigure)
