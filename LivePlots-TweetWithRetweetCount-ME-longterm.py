import os, sys, time, numpy as np, matplotlib.pyplot as plt, LivePlotConfig, LivePlotConfig
import csv
from datetime import datetime, date, timedelta
import matplotlib.dates as mdates

import tweetMiningLib as mLib


outputFolder=LivePlotConfig.outputFolder()

start_date = '2019-11-20' #mLib.daysAgo(5*30)
end_date = mLib.yesterday()
hashtags = ["#MECFS", "#MEawarenesshour", #"#pwME", "#MillionsMissing", "#MyalgicE", "#cfs", "#Spoonie", "#neisvoid"
                    "#LongCovid"
                    ]

runningAverageSpan = 7

saveFigure=True

plt.style.use(LivePlotConfig.pltStyle())
plt.subplots(figsize=LivePlotConfig.pltFigSize())

dataSets=[None]*(len(hashtags))
for i in range(len(hashtags)):
    retweetCountsPerTweetPerDay, dates = mLib.extractDataFromSavedTweets(hashtags[i], start_date, end_date, mLib._row_extract_retweetCount)
    retweetsPerDay = [sum(retweetCountsForOneDay) for retweetCountsForOneDay in retweetCountsPerTweetPerDay]
    TweetCount, dates = mLib.getTweetCountFromSavedTweets(hashtags[i], start_date, end_date)
    TweetsPlusRetweets = list(np.array(retweetsPerDay) + np.array(TweetCount))
    dataSets[i] = mLib.runningAverageDataset([mLib.datetimesFromStr(dates), TweetCount, TweetsPlusRetweets], runningAverageSpan)
    color = mLib.getHsvColor(i, len(dataSets))
    plt.fill_between(dataSets[i][0], dataSets[i][1], 0, color= (*(color[:3]), 0.08))
    plt.plot(dataSets[i][0], dataSets[i][1], c=color, alpha=0.25, linewidth=1)
    plt.plot(dataSets[i][0], dataSets[i][2], c=color, alpha=0.8, linewidth=1.8, label=hashtags[i])

#plt.xticks(rotation=90)

plt.subplots_adjust(left=0.050, bottom=0.08, right=0.985, top=0.92)
plt.legend();
plt.grid(which='major', alpha=0.35, linestyle='--');
plt.ylabel('tweets + retweets per day' + (' with ' + hashtags[0] if len(dataSets)<=1 else '') + ' (' + str(runningAverageSpan) + ' d avg)');
#plt.yscale('symlog')
plt.title("Vergleich ausgewählter Twitter Hashtags (Langzeit)")

if saveFigure:
    FilePath = outputFolder + 'Tweet+Retweet count ' + ' '.join(hashtags).replace('#','') + ' longterm' + LivePlotConfig.fileType()
    plt.savefig(FilePath, dpi=LivePlotConfig.dpi())
    print('Saved Figure: '+ FilePath)
#plt.show();
