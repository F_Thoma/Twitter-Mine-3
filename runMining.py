import os, sys, time, numpy as np, matplotlib.pyplot as plt
import csv
from datetime import date, timedelta, datetime

import tweetMiningLib as mLib

print("Current time: " + datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

def doMining(start_date, end_date, hashtag):
    outputFolder=mLib.get_folder_name(hashtag)
    if not os.path.exists(outputFolder):
        os.makedirs(outputFolder)
        print('Created missing folder: ' + outputFolder)

    mLib.saveTweetsInDaterange(hashtag, start_date, end_date)

##
end_date =  mLib.yesterday() #(inklusive: the end date will be reached)
start_date = mLib.daysAgo(7) #data thats older than 7 days is not provided by the twitter API without payment

hashtags = [
    "#MECFS"
    ,"#MEawarenesshour"
    ,"#LongCovid"
    ,"#pwME", "#MillionsMissing", "#MyalgicE", "#cfs", "#Spoonie", "#neisvoid", "#MemesForME", "#EndMeCfs2021", "#MyalgicEncephalomyelitis", "#ChronicFatigueSyndrome", "#MEAwarenessDay", "#MEAwarenessWeek", "#MEAwarenessMonth", "#MillionsMore", "#LightUptheNight4ME", "#LightUptheNightforME", "#SIGNforMECFS", "#BC007"
    ,"#chronicCovid", "#longHaulers", "#longHauler", "#StillSickStillFighting"
                    #,"#linksextrem", "#rechtsextrem"
                    #,"#Amthor", "#Korruption", "#Lobby", "#Lobbyismus"
                    #,"#grippe"
		    #, "#Merkel"
		    #, "#Trump"
            ]

for hashtag in hashtags:
    doMining(start_date, end_date, hashtag)
    print ('Finished ' + hashtag +'\n')

##
print("\n\nSuccessfully finished mining!\n\n")
