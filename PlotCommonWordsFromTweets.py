import os, sys, time, numpy as np, matplotlib.pyplot as plt
import csv
from datetime import datetime
import matplotlib.dates as mdates
from collections import Counter

import tweetMiningLib as mLib

start_date = '2017-01-01'
end_date = '2020-08-15'
hashtag = '#MECFS'
saveFigure=False
PlotItemCount = 10
PrintItemCount = 50
runAvgCount = 42


print(datetime.now())
print('Reading Files...')
wordCounterPerDayPerTweet, dates = mLib.extractDataFromSavedTweets(hashtag, start_date, end_date, mLib._row_extract_wordCount)

print('Counting...')
wordCounterPerDay = [mLib.counterSum(wordCounterPerTweetOnOneDay) for wordCounterPerTweetOnOneDay in wordCounterPerDayPerTweet]
totalCounter = mLib.counterSum(wordCounterPerDay)

print(datetime.now())
print('Removing the word '+ hashtag +' from statistics')
totalTweetCount = totalCounter[hashtag.lower()]
del totalCounter[hashtag.lower()]
print('  There are ' + str(len(list(totalCounter))) + ' unique words used')
i=0
for counter in wordCounterPerDay:
    del counter[hashtag.lower()]
    i+=1

print('Top words (with boring ones): ')
for otherHashtag, usage in totalCounter.most_common(PrintItemCount):
    print(round (usage/totalTweetCount*100, 3), '% ', usage, otherHashtag)
print()

boringCommonWords= ['the', 'to', 'of', 'and', 'a', 'for', 'in', 'is', 'i', 
	'with', 'are', 'this', 'have', 'you', 'that', 'on', 'not', 
	'it', 'be', 'we', 'my', 'from', 'but', 'as', 'about', 'so', '-', '&amp;', 'if', 'many', 'been', 'or', 'who', 
	'will', 'at', 'by', 'me', 'more', 'de', 'your', 'can', 'what', 'all', 'our', 'just', 'an', 'has', 'like', 
	'was', 'they', 'some', 'there', 'no', 'need', 'get', 'had', 'how', 'now', 
	'know', 'those', "it's", 'do', 'after', 'out', 'very', 'us', 'also', 'up', 'it’s', 'their', 'being', 'one', 'much', 
	'day', 'when', "i'm", '&', 'der', 'even', 'would', 'la', 'see', 'en', 'may', 'could', "don't", 'group', 'i’m', 'only', 'than', 
	'really', 'und', 'other', 'think', 'ist', 'que', 'new', 'am', 'going', 'which', 'into', 'these', 'were', 'any', 'les', 'mit', 'read', 'et', 'für', 'because',
        '|', 'zu', 'es', 'das', 'ich', 'nicht', 'von', 'why', '9', 'go', 'her'
	]
#'los', 'thread', 'facebook', 'over', 'effects', '#myalgice', 'des', 'why', 'long-term', 'don’t', 'thanks', 'illness', 'should', 'too', 'take', 'weeks', 'since', 
#'great', 'getting', 'symptoms.', 'want', 'important', '#apresj90', 'thousands', 'life', 'experiencing', '#covid1in10', 'recovery', '#coronavirus', 'le', 'work', 
#'same', 'got', 'es', 'online', 'go', 'them', 'week', 'having', "i've", 'look', 'viral', '#apresj60', 'podcast', 'might', 'here', 'most', 'experience', 'days', 'à', 
#'article', 'well', '#コロナ後遺症', 'better', 'disease', 'this.', 'cases', 'through', 'je', 'care', '#apresj120', 'first', '#millionsmissing', 'zum', 'me/cfs', '.', 
#'#cfs', 'zur', 'lot', 'awareness', 'last', 'today', 'bm', '#covidー19', 'ill', 'uk', 'debilitating', '#koronaoire', '@anjakarliczek', 'heart', 'neue', 'aktuellen', 
#'then', 'sick', 'ms', 'erstmals', 'it.', 'wissenschaft', 'http://bit.ly/337l006', 'impulse.“', '#dasistbioökonomie', '„karliczek.', 'leinen', 'ausgabe', '@_mswiss', 
#'diskussionsrunde', '#bioökonomie', 'da!', '„faktensammler“', 'hören:', '#newsletter', '|', 'never', 'doctors', 'issues', '#covid_19', 'he', '2', 'needs', 'damage', 
#'i’ve', '#長期微熱組', '3', 'exercise', '#covid', '?', 'its', 'doing', 'patient', 'pas', 'nicht', 'anyone', 'patients.', 'symptoms,', 'understand', 'does', 'few', 
#'months.', '4', 'un', 'another', 'study', 'make', '@meassociation', '5', 'share', 'find', 'das', 'post-viral', 'pour', 'her', 'young', 'already', 'via', 'made', 
#'healthy', 'community', 'zu', 'brain']
#   print([ pair[0] for pair in totalCounter.most_common(300)]) # get common words to copy to boringCommonWords
print('Filter out boring words (e.g.' + ', '.join(boringCommonWords[:8])+ ')...')
for boringWord in boringCommonWords:
    del totalCounter[boringWord]
    for counter in wordCounterPerDay:
        del counter[boringWord]

print('Top words (without boring ones): ')
for otherHashtag, usage in totalCounter.most_common(PrintItemCount):
    print(round (usage/totalTweetCount*100, 3), '% ', usage, otherHashtag)
print()

print('Formatting data for plot...')
def getDataFormatted(counterPerDay, totalCounter, tweetCountPerDay, MaxCount =None):
    days = len(counterPerDay)
    data = dict(totalCounter.most_common(MaxCount))
    
    for word,trash in totalCounter.most_common(MaxCount):
        data[word] = np.zeros(days)
    for i in range(days):
        for word,trash in totalCounter.most_common(MaxCount):
            data[word][i] = (counterPerDay[i][word]/tweetCountPerDay[i] if tweetCountPerDay[i] is not 0 else -1)
    return data
def removeWrongData(data):
    for i in range(len(data[1])-1, -1, -1):
        if data[1][i] < 0:
            data = np.delete(data, i, 1)
    return data

tweetCountPerDay, dates = mLib.getTweetCountFromSavedTweets(hashtag, start_date, end_date)
data = getDataFormatted(wordCounterPerDay, totalCounter,tweetCountPerDay, PlotItemCount)

print('Plotting data...')
plt.figure(figsize=(10,5))

cmap = plt.get_cmap('hsv')
i=0
for word, wordCount in totalCounter.most_common(PlotItemCount):
    relativeTweetCount = data[word]
    dataSet = [mLib.datetimesFromStr(dates, endOffset=-1), relativeTweetCount*100]
    dataSet = removeWrongData(dataSet)
    runAvgDataSet = mLib.runningAverageDataset(dataSet, runAvgCount)
    color = cmap(float(i)/len(data))
    if(len(dataSet[0]) < 200):
        plt.plot(dataSet[0], dataSet[1], c=color, alpha=0.12, linewidth=0.5)#, label=word)
    plt.plot(runAvgDataSet[0], runAvgDataSet[1], '--' if i%2 else '', c=color, alpha=0.8, linewidth=1.8,
             label= str(round(totalCounter[word]/totalTweetCount*100, 1)) +'% '+word)
    i+=1

#plt.xaxis.set_major_locator(mdates.MonthLocator())
#plt.xaxis.set_major_formatter(mdates.DateFormatter('%b %y'))
plt.xticks(rotation=90)

plt.subplots_adjust(left=0.065 , bottom=0.20, right=0.77, top=0.95)
#plt.legend()
plt.legend(loc='upper left', bbox_to_anchor=(1.0, 1.0));
plt.grid(which='major', alpha=0.35, linestyle='--');
plt.ylabel('top '+ str(PlotItemCount) +' nonusual words used with ' + hashtag + ' in %  ('+str(runAvgCount)+'d avg)');
if saveFigure:
    plotFilepath = mLib.get_tweets_per_day_generated_plot_file_path(hashtags, start_date, end_date)
    plt.savefig(plotFilepath)
#plt.yscale('symlog')
plt.title(hashtag)

print(datetime.now())
plt.show();
