# Twitter Mine 3

Save posts from twitter and analyze the data.

_This program requires a Twitter account to work._

## Enter Credentials

Make a copy of `Twitter.credentials.example` and rename it to `Twitter.credentials`. Open it in a text editor and enter the personal tokens from your developer account.

## Dependencies

- tweepy: [GitHub](https://github.com/tweepy/tweepy), [Homepage](https://www.tweepy.org/), Installation: `pip install tweepy`

More dependencies: [`dependencies.txt`](./dependencies.txt).

## License

See [`LICENSE`](./LICENSE) and [`gpl-3.0.md`](./gpl-3.0.md).