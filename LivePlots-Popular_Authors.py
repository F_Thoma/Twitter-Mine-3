import os, sys, time, numpy as np, matplotlib.pyplot as plt, LivePlotConfig
import csv
from datetime import datetime, date, timedelta
import matplotlib.dates as mdates
from collections import Counter

import tweetMiningLib as mLib

def popularAuthorsPlot(hashtag,
                       outputFolder,
                       start_date,
                       end_date,
                       PlotCount,
                       PrintCount,
                       runAvgCount):
    saveFigure=True
    
    PopularityPerAuthorCounterPerDayPerTweet, dates = mLib.extractDataFromSavedTweets(hashtag, start_date, end_date, mLib._row_extract_popularity_per_author)

    CounterPerDay = [mLib.counterSum(CountersOfOneDay) for CountersOfOneDay in PopularityPerAuthorCounterPerDayPerTweet]
    totalCounter = mLib.counterSum(CounterPerDay)
    totalPopularity = sum(totalCounter.values())

    if PrintCount>0:
        print('Hashtag:', hashtag)
        print('Time:', start_date, 'until', end_date)
        print('There are ' + str(len(list(totalCounter))) + ' authors and ' + str(totalPopularity) + ' tweets')
        print('Top authors: ')
        print('Nr.', 'Amount (rel., abs.)', 'author', sep='\t')
        i =1
        for item, quantity in totalCounter.most_common(PrintCount):
            print('['+str(i)+']', round (quantity/totalPopularity*100, 3), '% ', quantity, item, sep='\t')
            i+=1
        print()

    def removeWrongData(data):
        for i in range(len(data[1])-1, -1, -1):
            if data[1][i] < 0:
                data = np.delete(data, i, 1)
        return data

    popularitySumPerDay = [sum(counterOfOneDay.values()) for counterOfOneDay in CounterPerDay]
    data           = mLib.getRelativeAmountFromCounters(CounterPerDay, totalCounter, popularitySumPerDay, PlotCount)
    runningAvgData = mLib.getRelativeAmountFromCounters(CounterPerDay, totalCounter, popularitySumPerDay, PlotCount, avgCount=runAvgCount)

    plt.style.use(LivePlotConfig.pltStyle())
    plt.subplots(figsize=LivePlotConfig.pltFigSize())

    i=0
    for item, quantity in totalCounter.most_common(PlotCount):
        dataSet = [mLib.datetimesFromStr(dates), data[item]*100]
        dataSet = removeWrongData(dataSet)
        runAvgDataSet = [mLib.runAvgXcompensation(mLib.datetimesFromStr(dates), runAvgCount), runningAvgData[item]*100]
        runAvgDataSet = removeWrongData(runAvgDataSet)
        color = mLib.getHsvColor(i, len(data))
        if(len(dataSet[0]) < 60):
            plt.plot(dataSet[0], dataSet[1], c=color, alpha=0.2, linewidth=0.7)#, label=item)
        plt.plot(runAvgDataSet[0], runAvgDataSet[1], '--' if i%2 else '', c=color, alpha=0.8, linewidth=1.8,
                 label= str(round(totalCounter[item]/totalPopularity*100, 1)) +'% '+item)
        i+=1


    #plt.xticks(rotation=90)

    plt.subplots_adjust(left=0.050 , bottom=0.08, right=0.83, top=0.94)
    plt.legend(loc='upper left', bbox_to_anchor=(1.0, 1.0));
    plt.grid(which='major', alpha=0.35, linestyle='--');
    plt.ylabel('popularity of top '+ str(PlotCount) +' authors in %  ('+str(runAvgCount)+'d avg)');
    #plt.yscale('symlog')
    plt.title('Popular Authors of ' + hashtag)
    print(datetime.now())
    if saveFigure:
        FilePath = outputFolder + 'Popular Authors of ' + hashtag.replace('#','') + LivePlotConfig.fileType()
        plt.savefig(FilePath, dpi=LivePlotConfig.dpi())
        print('Saved Figure: '+ FilePath)
    else:
        plt.show();

if __name__ == "__main__":
    start_dates = ['2020-05-01', '2020-05-01', '2020-06-28']
    end_date = mLib.yesterday()

    for i, hashtag in enumerate(['#MECFS', '#MEawarenesshour', '#LongCovid']):
        popularAuthorsPlot(hashtag = hashtag,
                           outputFolder = LivePlotConfig.outputFolder(),
                           start_date = start_dates[i],
                           end_date = end_date,
                           PlotCount = 12,
                           PrintCount=30,
                           runAvgCount=7*1)
