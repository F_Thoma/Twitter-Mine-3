import os, sys, time, numpy as np, matplotlib.pyplot as plt, csv, matplotlib.ticker as ticker, LivePlotConfig
from datetime import timedelta, date, datetime
from collections import Counter

import tweepy

twitterApi = None
non_bmp_map = dict.fromkeys(range(0x10000, sys.maxunicode + 1), 0xfffd)
lineTerminator = '\n'

## basic functions
def listSum(myList):
    SumResult = list()
    for item in myList:
        SumResult += item
    return SumResult
def counterSum(myList):
    SumResult = Counter()
    for item in myList:
        SumResult += item
    return SumResult


## get tweets from server
def generateSpamFilterFunction(hashtag):
    lowerTag = hashtag.lower()
    def filterFunction(tweet):
        lowerTweetTags = [tag["text"].lower() for tag in tweet.entities["hashtags"]]
        if len(lowerTweetTags) == 0:
            return False;
        containsTag = lowerTag in lowerTweetTags
        if not containsTag:
            tagString = ' '.join(lowerTweetTags)
            print("Spam: " + tweet.id_str +" "+ tagString + " " + tweet.text)
        return containsTag
    return filterFunction


def setupTwitterApi():
    global twitterApi
    rel_path ="Twitter.credentials"
    lines = None
    try:
        script_dir = os.path.dirname(__file__)
        path = os.path.join(script_dir, rel_path)
        file = open(path)
        lines = file.readlines()
        file.close()
    except Exception as e:
        print("Exception reading file: " + path)
        print(e)
        return False

    for i in range(len(lines)):
        line = lines[i]
        if line != "":
            parts = line.split(':');
            var_name = parts[0].strip().lower()
            value = parts[1].strip()
            if var_name == "api key":
                consumer_key= value
            elif var_name == "api secret":
                consumer_secret  = value
            elif var_name == "access token key": 
                access_token  = value
            elif var_name == "access token secret": 
                access_token_secret  = value
            else:
                print("Error while reading the Twitter.credentials file. The name of the variable in line " + str(i + 1) + " does not match any of the expected names.");
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    twitterApi = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
    return True

def getTweets(hashtag:str, until=None, since_id=None, filtered: bool = False):
    global twitterApi
    if twitterApi is None:
        if not setupTwitterApi():
            return False
        
    response = tweepy.Cursor(twitterApi.search, q=hashtag, since_id=since_id, until=until, result_type= 'recent', count = 100).items()
    
    rawtweets = list()
    for tweet in response:
        rawtweets.append(tweet)
    
    if filtered:
        tweets = list(filter(generateSpamFilterFunction(hashtag), rawtweets))
    else:
        tweets = rawtweets
    print( "For " + hashtag + " since_id "+str(since_id)+" until "+until+": "+str(len(tweets))+" tweets." ) #TODO
    return tweets
  
## date (time)
def daterange(start_date, end_date, step : int= 1):
    start_date = datetime.strptime(start_date, "%Y-%m-%d")
    end_date   = datetime.strptime(end_date,   "%Y-%m-%d")
    result = []
    for n in range(int((end_date - start_date).days), -1, -step):
        result.append((start_date + timedelta(n)).strftime("%Y-%m-%d"))
    return result
def datetimesFromStr(dates, startOffset:int=0, endOffset:int=0):
    return [datetime.strptime(dates[i],'%Y-%m-%d') for i in range(startOffset, len(dates) + endOffset)]
def daysAgo(x:int):
    return (date.today() - timedelta(days = x)).strftime("%Y-%m-%d")
def yesterday():
    return daysAgo(1)
def twoMonthsAgo():
    return daysAgo(62)

## filesystem names
def get_folder_name(hashtag:str):
    return hashtag.replace('#','') + os.sep

def get_tweet_file_path(hashtag:str, date:str):
    return get_folder_name(hashtag) + date + " "+hashtag.replace('#','')+".csv"

def get_tweets_per_day_file_path(hashtag:str, start_date:str, end_date:str):
    return get_folder_name(hashtag) + 'TweetsPerDay ' + start_date+' to ' + end_date + " "+hashtag.replace('#','')+".csv"

def get_tweets_per_day_generated_plot_file_path(hashtag:str, start_date:str, end_date:str):
    return get_folder_name(hashtag) + 'TweetsPerDay ' + start_date+' to ' + end_date + " "+hashtag.replace('#','')+"autogeneratd.png"

## saving
def saveTweets(outputFileName: str, tweets):
    finished = False
    with open(outputFileName, "w", encoding='utf-8') as outputFile:
        csvFile = csv.writer(outputFile, lineterminator=lineTerminator)
        csvFile.writerow(['date', 'username', 'to', 'replies (unavailable)', 'retweets',
                          'favorites', 'text', 'geo (deprecated)', 'mentions', 'hashtags',
                          'id', 'permalink'])
        for t in tweets:
            rowdata = [t.created_at, #.strftime("%Y-%m-%d %H:%M:%S")
                t.user.screen_name,
                t.in_reply_to_screen_name  or '',
                '',
                t.retweet_count,
                t.favorite_count,
                t.text.replace(lineTerminator, "").replace("\r",""),
                '',
                [user.screen_name for user in t.contributors] if t.contributors is not None else '',
                ' '.join([tag["text"] for tag in t.entities["hashtags"]]),
                t.id_str,
                "https://twitter.com/" + t.user.screen_name + "/status/" + t.id_str]

            csvFile.writerow(map(str, rowdata))
        finished = True
    if finished:
        print('saved: ' + outputFileName + ' with ' + str(len(tweets)) + ' entries.')
    else:
        print('Unknown error - did not save properly (' + outputFileName + ' with ' + str(len(tweets)) + ' entries).')
    return finished

def saveTweetsPerDay(outputFileName: str, dates, TweetCount):
    with open(outputFileName, "w", encoding='utf-8') as outputFile:
        csvFile = csv.writer(outputFile, lineterminator=lineTerminator)
        csvFile.writerow(['date', 'tweets'])
        for i in range(len(TweetCount)):
            csvFile.writerow([dates[i], TweetCount[i]])
        print('saved: ' + outputFileName + ' with ' + str(len(TweetCount)) + ' entries.')

## loading
def file_line_count(fname):
    with open(fname, encoding='utf-8') as f:
        for i, l in enumerate(f):
            pass
    return i + 1

def getTweetCountFromSavedTweets(hashtag, start_date, end_date):
    dates = daterange(start_date, end_date)

    TweetCount = []
    missingDataIndices = list()
    for i in range(len(dates)):
        inputFilename = get_tweet_file_path(hashtag, dates[i])
        if os.path.isfile(inputFilename):
            TweetCount.append(file_line_count(inputFilename) -1)
        else:
            missingDataIndices.append(i)
    while len(missingDataIndices)>0:
        del dates[missingDataIndices.pop()]
    return (TweetCount, dates)

def _row_extract_hashtags(row):
    #return [x.lower() for x in row[9].split(' ')]
    return row[9].split(' ')
def _row_extract_wordCount(row):
    return Counter(row[6].lower().split())
def _row_extract_author(row):
    return row[1]
def _row_extract_replyCount(row):
    if row[3] == '':
        return 0;
    else:
        return int(row[3]);
def _row_extract_retweetCount(row):
    if row[4] == '':
        return 0;
    else:
        return int(row[4]);
def _row_extract_favoriteCount(row):
    if row[5] == '':
        return 0;
    else:
        return int(row[5]);
def _row_extract_id(row):
    return row[10]
def _row_extract_text(row):
    return row[6];
def _row_extract_datetime(row):
    return datetime.strptime(row[0], "%Y-%m-%d %H:%M:%S")

def _row_extract_popularity(row):
    return _row_extract_retweetCount(row) + _row_extract_favoriteCount(row)
def _row_extract_popularity_per_author(row):
    return Counter({_row_extract_author(row): _row_extract_popularity(row)})
def _generate_popularity_extractor(minimumPopularity:int = 3):
    def _row_extract_popularity_and_content(row):
        popularity = _row_extract_popularity(row)
        if popularity >= minimumPopularity:
            return (popularity, row)
        else:
            return None
    return _row_extract_popularity_and_content

def extractDataFromSavedTweetsOneDay(hashtag:str, date:str, rowExtractData):
    filePath = get_tweet_file_path(hashtag, date)  
    with open(filePath, "r", encoding='utf-8') as inputFile:
        csvFile = csv.reader(inputFile, lineterminator=lineTerminator)
        firstRow = True
        data = []
        for row in csvFile:
            if firstRow:
                header = row
                firstRow = False
            else:
                #print(str(row).translate(non_bmp_map))
                data.append(rowExtractData(row))
    return data
def extractDataFromSavedTweets(hashtag:str, start_date:str, end_date:str, rowExtractData, removeMissingDates = True):
    dates = daterange(start_date, end_date)
    outputDates = []
    extractedData = []
    for i in range(len(dates)):
        inputFilename = get_tweet_file_path(hashtag, dates[i])
        if os.path.isfile(inputFilename) or not removeMissingDates:
            extractedData.append(extractDataFromSavedTweetsOneDay(hashtag,dates[i],rowExtractData))
            outputDates.append(dates[i])
    return (extractedData, outputDates)


def readTweetsPerDay(FileName):
    with open(FileName, "r", encoding='utf-8') as inputFile:
        csvFile = csv.reader(inputFile, lineterminator=lineTerminator)
        firstRow = True
        data = [[],[]]
        for row in csvFile:
            if firstRow:
                header = row
                firstRow = False
            else:
                data[0].append(row[0])
                data[1].append(int(row[1]))
    return(data, header)

## mining
def saveTweetsInDaterange(hashtag, start_date: str, end_date: str, debug: bool = False):
    print('Getting Tweets for ' +hashtag + ' from ' + start_date + ' to '+ end_date + ' ...')
    dates = daterange(start_date, end_date)
    filePaths = [get_tweet_file_path(hashtag, date) for date in dates]
    fileExists = [os.path.isfile(filePath) for filePath in filePaths]
    
    skippingStart=None # remember when dates are skipped to print a compact message
    i = 0
    while i<len(dates)-1:
        if fileExists[i]:
            if skippingStart is None:
                skippingStart=dates[i]
            i += 1
            continue
        if skippingStart is not None:
            print('Skipped ' + skippingStart +' until ' + dates[i-1] + ' (was already downloaded)')
            skippingStart=None

        j = i #j is the index of the next date for which the data was already downloaded 
        while (j < len(dates)-1) and not fileExists[j]:
            j+=1
        since_id = ''
        if fileExists[j] and file_line_count(filePaths[j])>1:
            since_id = max(extractDataFromSavedTweetsOneDay(hashtag, dates[j], _row_extract_id))
        else:
            j+=1 # j is excluded (one day before the first day)
        day_after_last_date = (datetime.strptime(dates[i], "%Y-%m-%d") + timedelta(1)).strftime("%Y-%m-%d")
        
        mixedTweets = getTweets(hashtag, until= day_after_last_date, since_id= since_id);
        sortedTweets = [None]*(j-i)
        dateToIndex = dict()
        for k in range(i,j):
            dateToIndex[dates[k]] = k-i
            sortedTweets[k-i] = list()
        for tweet in mixedTweets:
            if not tweet.retweeted and not tweet.text.startswith('RT @'):
                day = tweet.created_at.strftime("%Y-%m-%d")
                if day in dateToIndex:
                    sortedTweets[dateToIndex[day]].append(tweet)
            
        for k in range(i,j):
            if debug:
                printOverview(sortedTweets[k-i])
            saveTweets(filePaths[k], sortedTweets[k-i])
        i=j


## print tweets
def printOverview (tweets):
    n = 0
    for tweet in tweets:
        print('[' + str(n)+']' + str(tweet.created_at) +' '
              #+ str(tweet.favorites) +' '
              #+ tweet.text[0:70].translate(non_bmp_map)
              + ' '.join([tag["text"] for tag in tweet.entities["hashtags"]])
        )
        n += 1
        
def numberVisualization(number, min_number, max_number, subdivisions:int, lineChar='.', posChar='v'):
    subdivisions -=1
    value = round((number - min_number)*subdivisions/(max_number-min_number))
    string = lineChar*value + posChar + lineChar*(subdivisions-value)
    return string
def htmlDateVisualization(date, min_date, max_date, subdivisions, lineChar='.', posChar='v'):
    result =  "<span class=dateVisualization>" + numberVisualization(date, min_date, max_date, subdivisions, lineChar, posChar)
    result += "<span class=dateVisualizationTooltip>" + date.strftime("%d.%m.%y %H:%M")
    result += "</span></span>"
    return result

def savePopularTweetsInHtmlFile(hashtag, outputFolder, start_date, end_date, maxTweetCount, minimumPopularity, printToConsole=False, filenameSuffix=''):
    print(datetime.now())
    print('  Reading Files...')
    popularityAndRowsPerDay, dates = extractDataFromSavedTweets(hashtag, start_date, end_date, _generate_popularity_extractor(minimumPopularity))

    print('  Sorting...')
    popularityAndRowsAllTime = list()
    for dayData in popularityAndRowsPerDay:
        for popularityAndRow in dayData:
            if not popularityAndRow is None:
                popularityAndRowsAllTime.append(popularityAndRow)
    popularityAndRowsPerDay = None

    def sortKey(popularityAndRowTuple):
        return popularityAndRowTuple[0]
    popularityAndRowsAllTime.sort(key=sortKey, reverse=True)
    topTweetCount = min(maxTweetCount, len(popularityAndRowsAllTime))

    print('Hashtag:', hashtag)
    print('Time:', start_date, 'until', end_date)
    print('There are ' + str(len(popularityAndRowsAllTime)) + ' tweets with a popularity of at least ' + str(minimumPopularity))
       

    # save as file
    html = """
    <style type="text/css">
        a:link {color:#eee; text-decoration:none;}
        a:visited {color:#aba; text-decoration:none;}
        a:hover {text-decoration:underline;}
        body{
            background: #101010;
            color: #eee;
            font-family: sans-serif;
        }
        td{
            padding: 2px 10px;
        }
        table{
            white-space: nowrap;
        }
        .dateVisualization{
            position: relative;
        }
        .dateVisualizationTooltip{
            visibility: hidden;
            position: absolute;
            bottom: 110%;
            left: 105%;
            padding: 0.5rem;
            background-color: #393939;
            border-radius: 3px;
        }
        .dateVisualization .dateVisualizationTooltip::after {
            content: "";
            position: absolute;
            bottom: 0%;
            left: 0%;
            border-width: 5px;
            border-style: solid;
            border-color: transparent transparent #888 #888;
        }
        .dateVisualization:hover .dateVisualizationTooltip{
            visibility: visible;
        }
    </style>    
    """
    html += "<h4> Most Popular Tweets for " + hashtag + "</h4>\n"
    html += "<h5> Dates: " + start_date +' until ' + end_date + "</h5>\n"

    html += "<table>\n<tr>\n"
    for thText in ['Nr.', 'Sum', 'RT', 'Fav', 'Date', 'Author', 'Text']:
        html += "<th> " + thText + " </th>\n"
    html += "</tr>\n"
    start_date_as_date = datetime.strptime(start_date + " 00:00:00", "%Y-%m-%d %H:%M:%S")
    end_date_as_date = datetime.strptime(end_date + " 23:59:59", "%Y-%m-%d %H:%M:%S")
    for index, popularityAndRow in enumerate(popularityAndRowsAllTime[:topTweetCount]):
        row = popularityAndRow[1]
        html += "<tr>\n"
        for tdText in ['['+str(index+1)+']', popularityAndRow[0], row[4], row[5],
                       htmlDateVisualization(_row_extract_datetime(row), start_date_as_date, end_date_as_date, 20),
                       row[1], row[6]]:
            html += "<td><a href="+row[11]+' target="_blank" rel="noopener noreferrer"> ' + str(tdText) + " </a></td>\n"
        html += "</tr>\n"
    html+="</table>\n"

    filename = outputFolder + "Popular Tweets with "+hashtag.replace('#','')+filenameSuffix+".html"
    with open(filename, "w", encoding='utf-8') as f:
        f.write(html)
        print("Saved file at " + filename)

    # print to console
    if printToConsole:
        print('Top tweets: ')
        print('Nr.', 'Sum', 'RT', 'Fav', 'date\t\t', 'author\t', 'text', sep='\t')

        i =1
        for popularityAndRow in popularityAndRowsAllTime[:topTweetCount]:
            row = popularityAndRow[1]
            print('['+str(i)+']', popularityAndRow[0], row[4], row[5], row[0], row[1], row[6].translate(non_bmp_map), sep='\t')
            i+=1
        print()
    print()

  
## plot
def beginOfMonth(date):
    return (date[-2:] == '01')
def getHsvColor(index:int, length:int):
    cmap = plt.get_cmap('hsv')
    color = cmap(float(index)/length)
    color = tuple(part * LivePlotConfig.plotColorBrightness()+LivePlotConfig.plotColorGreyBrightness() for part in color) 
    return color

def plotTweetPerDayData(TweetCount, dates, hashtag, saveFigure: bool = False):
    data = [dates, TweetCount]
    dayCount1 = 3
    Smooth1Data = runningAverageDataset(data, dayCount1)
    dayCount2 = 14
    Smooth2Data = runningAverageDataset(data, dayCount2)

    plt.plot(data[0], data[1], label=hashtag, alpha=0.3)
    plt.plot(Smooth1Data[0], Smooth1Data[1], label=hashtag+' '+str(dayCount1) +'d avg')
    plt.plot(Smooth2Data[0], Smooth2Data[1], label=hashtag+' '+str(dayCount2) +'d avg', alpha=0.5)

    plt.xticks(rotation=90)

    plt.subplots_adjust(left=0.095 , bottom=0.20, right=0.985, top=0.95)
    plt.legend();
    plt.grid(which='major', alpha=0.35, linestyle='--');
    plt.ylabel('tweets per day' + (' with ' + hashtag if len(data)<=2 else ''));
    plt.title(hashtag)
    if saveFigure:
        plotFilepath = get_tweets_per_day_generated_plot_file_path(hashtag, start_date, end_date)
        plt.savefig(plotFilepath)
    plt.show();

## Datenverarbeitung - Average
def runningAverage(roughList, count):
    result = np.zeros(len(roughList) - count +1)
    for i in range(count):
        result = result + roughList[i:len(roughList) - count +1 +i]
    result = result/count
    return result
def runAvgXcompensation(xValues, count):
    start = round((count-1)/2)
    end = -(count-1) + round((count-1)/2)
    return xValues[start:end]
def runningAverageDataset(data, count):
    return [runAvgXcompensation(data[0], count), *[runningAverage(data[i], count) for i in range(1, len(data))]]
## Datenverarbeitung - counter
def getRelativeAmountFromCounters(counterPerDay, totalCounter, totalCountPerDay, MaxCount =None, avgCount = 1):
    days = len(counterPerDay)
    data = dict(totalCounter.most_common(MaxCount))
    for hashtag,trash in totalCounter.most_common(MaxCount):
        data[hashtag] = np.zeros(days + 1 - avgCount)
    avgTotalCountPerDay = runningAverage(totalCountPerDay, avgCount)
    for hashtag,trash in totalCounter.most_common(MaxCount):
        avgCountPerDay = runningAverage([counter[hashtag] for counter in counterPerDay], avgCount)
        for i in range(days + 1 - avgCount):
            data[hashtag][i] = (avgCountPerDay[i]/avgTotalCountPerDay[i] if not (avgTotalCountPerDay[i] == 0) else -1)
    return data
