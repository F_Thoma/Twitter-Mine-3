import os, sys, time, numpy as np, matplotlib.pyplot as plt
import csv
from datetime import datetime, date, timedelta
import matplotlib.dates as mdates
from collections import Counter

import tweetMiningLib as mLib, LivePlotConfig

start_dates = ['2018-01-01', '2020-11-05', '2020-12-31', '2021-01-24', '2019-07-01', '2021-05-06']
end_date = mLib.yesterday()
hashtagsByStartDate = [["#pwME", "#MillionsMissing", "#MyalgicE", "#cfs", "#Spoonie", "#neisvoid"],
            ["#MemesForME"],
            ["#EndMeCfs2021"],
            ["#MyalgicEncephalomyelitis", "#ChronicFatigueSyndrome"],
            ["#chronicCovid", "#longHaulers", "#longHauler"],
            ["#MEAwarenessDay", "#MEAwarenessWeek", "#MEAwarenessMonth", "#MillionsMore", "#LightUptheNight4ME", "#LightUptheNightforME"]]

maxTweetCount = 100
minimumPopularity = 10

for i, hashtags in enumerate(hashtagsByStartDate):
    for hashtag in hashtags:
        mLib.savePopularTweetsInHtmlFile(hashtag = hashtag,
                                 outputFolder = LivePlotConfig.outputFolder(),
                                 start_date = start_dates[i],
                                 end_date = end_date,
                                 maxTweetCount = maxTweetCount,
                                 minimumPopularity = minimumPopularity,
                                 filenameSuffix=' longterm')
