@ECHO OFF
cd "C:\Portable Programs\Meine_Skripte\Twitter-Mine-3"

echo Started at %date% - %time% | tee output.log

echo ^<br^> >> "A:\Informatik\Codeberg-F_Thoma\pages\Status.html"
echo Attempting Update: %date% - %time% ... >> "A:\Informatik\Codeberg-F_Thoma\pages\Status.html"

python "runMining.py" | tee output.log -a

python "LivePlots-TweetCount-ME.py" | tee output.log -a
python "LivePlots-TweetWithRetweetCount-ME.py" | tee output.log -a
python "LivePlots-TweetCount-ME-longterm.py" | tee output.log -a
python "LivePlots-TweetWithRetweetCount-ME-longterm.py" | tee output.log -a
python "LivePlots-Authors-MECFS.py" | tee output.log -a
python "LivePlots-Authors-LongCovid.py" | tee output.log -a
python "LivePlots-Authors-MEAwarenessHour.py" | tee output.log -a
python "LivePlots-Popular_Authors.py" | tee output.log -a
python "LiveResults-PopularTweets-aroundME-longterm.py" | tee output.log -a
python "LiveResults-PopularTweets-ME-longterm.py" | tee output.log -a
python "LiveResults-PopularTweets-ME-shortterm.py" | tee output.log -a
python "LivePlots-TweetCount-All-longterm.py" | tee output.log -a
python "LivePlots-TweetWithRetweetCount-All-longterm.py" | tee output.log -a

for /F "tokens=2" %%i in ('date /t') do set mydate=%%i
set mytime=%time%

echo Last Update: %date% - %time% >"A:\Informatik\Codeberg-F_Thoma\pages\Status.html"

echo Done at %date% - %time% | tee output.log -a

echo Committing in Git... | tee output.log -a
A:
cd "A:\Informatik\Codeberg-F_Thoma\pages\"
git commit -a -m "Update data" | tee output.log -a
git push origin main | tee output.log -a
echo Done. | tee output.log -a