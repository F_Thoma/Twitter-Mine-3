import os, sys, time, numpy as np, matplotlib.pyplot as plt
import csv
from datetime import datetime, date, timedelta
import matplotlib.dates as mdates
from collections import Counter

import tweetMiningLib as mLib, LivePlotConfig

start_dates = ['2020-01-01', '2018-01-01', '2020-05-01']
end_date = mLib.yesterday()
hashtags = ['#MECFS', '#MEawarenesshour', '#LongCovid']

maxTweetCount = 100
minimumPopularity = 15

for i, hashtag in enumerate(hashtags):
    mLib.savePopularTweetsInHtmlFile(hashtag = hashtag,
                                 outputFolder = LivePlotConfig.outputFolder(),
                                 start_date = start_dates[i],
                                 end_date = end_date,
                                 maxTweetCount = maxTweetCount,
                                 minimumPopularity = minimumPopularity,
                                 filenameSuffix=' longterm')
