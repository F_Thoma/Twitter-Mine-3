import os, sys, time, numpy as np, matplotlib.pyplot as plt
import csv
from datetime import datetime, date, timedelta
import matplotlib.dates as mdates
from collections import Counter

import tweetMiningLib as mLib, LivePlotConfig

start_date = mLib.daysAgo(7) #one week
end_date = mLib.yesterday()

minimumPopularity = 15

for hashtag in ['#MEawarenesshour']:
    mLib.savePopularTweetsInHtmlFile(hashtag = hashtag,
                                 outputFolder = LivePlotConfig.outputFolder(),
                                 start_date = start_date,
                                 end_date = end_date,
                                 maxTweetCount = 25,
                                 minimumPopularity = minimumPopularity,
                                 filenameSuffix=' shortterm')

for hashtag in ['#MECFS', '#LongCovid']:
    mLib.savePopularTweetsInHtmlFile(hashtag = hashtag,
                                 outputFolder = LivePlotConfig.outputFolder(),
                                 start_date = start_date,
                                 end_date = end_date,
                                 maxTweetCount = 50,
                                 minimumPopularity = minimumPopularity,
                                 filenameSuffix=' shortterm')

